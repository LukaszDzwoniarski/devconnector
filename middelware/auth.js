const jwt = require('jsonwebtoken');
const config = require('config');


module.exports = function (req, res, next) {
    //get token from header
    const token = req.header('x-auth-token');
    console.log(token);
    //check if no token 
    if (!token) {
        return res.status(401).json({msg: 'no token, authorization denied'});
    }

    //verify token
    try {
        const decode = jwt.verify(token, config.get('jwtToken'));
        req.user = decode.user;
        next();
    } catch (e) {
        console.error(e.message);
        res.status(401).json({msg: 'token is not valid'});
    }
}