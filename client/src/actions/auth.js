import axios from 'axios';
import {REGISTER_FAIL, REGISTER_SUCCESS} from "./types";
import {setAlert} from "./alert";

//Register user
export const register = ({name, email, password}) => async dispatch => {
    const newUser = {
        name,
        email,
        password
    }
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    const body = JSON.stringify(newUser);
    try {
        const res = await axios.post('/api/users', body, config);
        dispatch({
            type: REGISTER_SUCCESS,
            payload: res.data
        })
    } catch (e) {
        const errors = e.response.data.errors;

        if (errors) {
            errors.forEach(c => setAlert(c.msg, 'danger'));
        }
        dispatch({
            type: REGISTER_FAIL
        })
    }
}