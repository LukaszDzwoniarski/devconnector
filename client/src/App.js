import React, {Fragment} from 'react';
import './App.css';
import Navbar from "./components/layout/Navbar";
import Lending from "./components/layout/Lending";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Loging from "./components/auth/Loging";
import Register from "./components/auth/Register";
import {Provider} from 'react-redux';
import store from './store';
import Alert from "./components/layout/Alert";

const App = () =>
    <Provider store={store}>
        <Router>
            <Fragment>
                <Navbar/>
                <Route exact path='/' component={Lending}/>
                <section className="container">
                    <Alert/>
                    <Switch>
                        <Route exact path='/register' component={Register}/>
                        <Route exact path='/login' component={Loging}/>
                    </Switch>
                </section>
            </Fragment>
        </Router>
    </Provider>
export default App;
